package com.example.a2slamprj2eq02_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.model.Restaurant;

import java.util.ArrayList;
import java.util.Objects;

public class ArrayAdapterResto extends ArrayAdapter<Restaurant> {
    private final Context context;
    private final ArrayList<Restaurant> lesRestaurants;

    public ArrayAdapterResto(Context context, ArrayList<Restaurant> lesRestaurants) {
        super(context, R.layout.patern_resto, lesRestaurants);
        this.context = context;
        this.lesRestaurants = lesRestaurants;
    }

    static class ViewHolder {
        TextView nomResto;
        TextView addrResto;
        TextView villeResto;
        ImageView imgResto;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.patern_resto, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.nomResto = convertView.findViewById(R.id.patern_name);
            viewHolder.addrResto = convertView.findViewById(R.id.patern_addr);
            viewHolder.villeResto = convertView.findViewById(R.id.patern_cp_ville);
            viewHolder.imgResto = convertView.findViewById(R.id.patern_img);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Restaurant restaurant = lesRestaurants.get(position);
        viewHolder.nomResto.setText(restaurant.getNomR());

        if (!Objects.equals(restaurant.getNumAdr(), "null")) {
            viewHolder.addrResto.setText(restaurant.getNumAdr() + " " + restaurant.getVoiAdr());
        } else {
            viewHolder.addrResto.setText(restaurant.getVoiAdr());
        }

        viewHolder.villeResto.setText(restaurant.getCpR() + " " + restaurant.getVilleR());

        if(!restaurant.getLesPhotos().isEmpty()) {
            new DownloadImageTask(viewHolder.imgResto).execute("https://yaguxxxx.fr/PROJETS/resto_gr2/photos/" + restaurant.getLesPhotos().get(0).getCheminP());
        }
        if (position % 2 == 0) {
            convertView.setBackgroundColor(context.getResources().getColor(R.color.bg_resto_1));
        } else {
            convertView.setBackgroundColor(context.getResources().getColor(R.color.bg_resto_2));
        }

        return convertView;
    }
}


