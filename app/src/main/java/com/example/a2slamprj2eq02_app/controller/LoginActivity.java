package com.example.a2slamprj2eq02_app.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.api.API;
import com.example.a2slamprj2eq02_app.model.Utilisateur;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etMail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etMail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        Button btnLogin = findViewById(R.id.btnLogin);
        Button btnRegister = findViewById(R.id.btnSignin);

        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                login();
                break;
            case R.id.btnSignin:
                register();
                break;
        }
    }

    private void register() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    private void login() {
        String email = etMail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
            return;
        }

        String url = API.buildUrl(API.URL_USERS_CONNECT, "mailU=" + email, "mdpU=" + password);
        System.out.println(url);
        Call<JsonObject> call = API.getJsonData(url);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject jsonObject = response.body();
                    boolean success = jsonObject.get("success").getAsBoolean();
                    if (success) {
                        Toast.makeText(LoginActivity.this, "Connexion réussie.", Toast.LENGTH_SHORT).show();
                        Utilisateur utilisateur = new Utilisateur(jsonObject.get("user").getAsJsonObject());
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        intent.putExtra("user", utilisateur);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Adresse email ou mot de passe incorrect.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // Gérez les erreurs de réponse ici
                    Toast.makeText(LoginActivity.this, "Erreur lors de la connexion. Veuillez réessayer.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Gérez les erreurs de réseau ici
                Toast.makeText(LoginActivity.this, "Erreur réseau. Veuillez vérifier votre connexion et réessayer.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}