package com.example.a2slamprj2eq02_app.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.a2slamprj2eq02_app.R;

public class LaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        android.content.Intent intent = new android.content.Intent(LaunchActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                },
                3000);
    }
}