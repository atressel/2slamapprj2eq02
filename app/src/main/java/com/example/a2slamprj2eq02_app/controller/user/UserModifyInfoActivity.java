package com.example.a2slamprj2eq02_app.controller.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.api.API;
import com.example.a2slamprj2eq02_app.controller.LoginActivity;
import com.example.a2slamprj2eq02_app.controller.SettingsActivity;
import com.example.a2slamprj2eq02_app.model.Utilisateur;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserModifyInfoActivity extends AppCompatActivity {

    private Utilisateur utilisateur;
    private EditText txtEmail;
    private EditText txtTel;
    private EditText txtNom;
    private EditText txtPrenom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_modify_info);

        initializeViews();
    }

    private void initializeViews() {
        Button btnModify = findViewById(R.id.btnModify);
        Button btnCancel = findViewById(R.id.btnCancel);

        utilisateur = (Utilisateur) getIntent().getSerializableExtra("user");

        txtEmail = findViewById(R.id.editTextEmail);
        txtTel = findViewById(R.id.editTextTelephone);
        txtNom = findViewById(R.id.editTextFirstName);
        txtPrenom = findViewById(R.id.editTextLastName);

        txtEmail.setText(utilisateur.getMailU());
        txtTel.setText(utilisateur.getTelU());
        txtNom.setText(utilisateur.getNomU());
        txtPrenom.setText(utilisateur.getPrenomU());

        btnCancel.setOnClickListener(v -> {
            Intent intent = new Intent(UserModifyInfoActivity.this, SettingsActivity.class);
            intent.putExtra("user", utilisateur);
            startActivity(intent);
        });

        btnModify.setOnClickListener(v -> modifyUser());
    }

    private void modifyUser() {
        String email = txtEmail.getText().toString();
        String tel = txtTel.getText().toString();
        String nom = txtNom.getText().toString();
        String prenom = txtPrenom.getText().toString();

        if (email.isEmpty() || tel.isEmpty() || nom.isEmpty() || prenom.isEmpty()) {
            Toast.makeText(UserModifyInfoActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
        } else if (email.equals(utilisateur.getMailU()) && tel.equals(utilisateur.getTelU()) && nom.equals(utilisateur.getNomU()) && prenom.equals(utilisateur.getPrenomU())) {
            Toast.makeText(UserModifyInfoActivity.this, "Aucune modification n'a été effectuée", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(UserModifyInfoActivity.this, SettingsActivity.class);
            intent.putExtra("user", utilisateur);
            startActivity(intent);
        } else {
            String url = API.buildUrl(API.URL_USERS_MODIFY, "idU=" + utilisateur.getIdU() + "&mailU=" + email + "&telU=" + tel + "&nomU=" + nom + "&prenomU=" + prenom);
            Call<JsonObject> callGetResto = API.getJsonData(url);
            callGetResto.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        JsonObject jsonObject = response.body();
                        if (jsonObject.get("success").getAsBoolean()) {
                            utilisateur.setMailU(email);
                            utilisateur.setTelU(tel);
                            utilisateur.setNomU(nom);
                            utilisateur.setPrenomU(prenom);

                            Toast.makeText(UserModifyInfoActivity.this, "Modification effectuée", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UserModifyInfoActivity.this, SettingsActivity.class);
                            intent.putExtra("user", utilisateur);
                            startActivity(intent);
                        } else {
                            Toast.makeText(UserModifyInfoActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(UserModifyInfoActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}