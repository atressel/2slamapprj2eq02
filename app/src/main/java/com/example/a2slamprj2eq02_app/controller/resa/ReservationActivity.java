package com.example.a2slamprj2eq02_app.controller.resa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.api.API;
import com.example.a2slamprj2eq02_app.controller.HomeActivity;
import com.example.a2slamprj2eq02_app.controller.SettingsActivity;
import com.example.a2slamprj2eq02_app.controller.user.UserModifyInfoActivity;
import com.example.a2slamprj2eq02_app.model.Utilisateur;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        Utilisateur user = (Utilisateur) getIntent().getSerializableExtra("user");
        int idResto = getIntent().getIntExtra("idResto", 0);

        Button btnReserver = findViewById(R.id.btnReserver);

        CalendarView calendarView = findViewById(R.id.resaCalendar);
        TimePicker timePicker = findViewById(R.id.resaHour);
        timePicker.setIs24HourView(true);

        TextView nomResto = findViewById(R.id.resa_name_resto);
        EditText nbPersonneValue = findViewById(R.id.nbPersonneValue);
        EditText nomReservation = findViewById(R.id.nomReservation);

        String url = API.buildUrl(API.URL_RESTAURANTS_GET, "idR=" + idResto);
        Call<JsonObject> callGetResto = API.getJsonData(url);
        callGetResto.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject jsonObject = response.body();
                    if (jsonObject.get("success").getAsBoolean()) {
                        JsonObject resto = jsonObject.get("resto").getAsJsonObject();
                        nomResto.setText(resto.get("nomR").getAsString());
                    } else {
                        Toast.makeText(ReservationActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(ReservationActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
            }
        });

        btnReserver.setOnClickListener(v -> {
            String date = calendarView.getDate() + "";
            String hour = timePicker.getHour() + "";
            String minute = timePicker.getMinute() + "";
            String nbPersonne = nbPersonneValue.getText().toString();
            String nomResa = nomReservation.getText().toString();

            String urlResa = API.buildUrl(API.URL_RESERVATION_ADD, "idU=" + user.getIdU() + "&idR=" + idResto + "&date=" + date + "&heure=" + hour + ":" + minute + "&nbPersonnes=" + nbPersonne + "&nomReservation=" + nomResa);
            Call<JsonObject> callPostResa = API.getJsonData(urlResa);
            callPostResa.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        JsonObject jsonObject = response.body();
                        if (jsonObject.get("success").getAsBoolean()) {
                            Toast.makeText(ReservationActivity.this, "Réservation effectuée", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ReservationActivity.this, HomeActivity.class);
                            intent.putExtra("user", user);
                            startActivity(intent);
                        } else {
                            Toast.makeText(ReservationActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(ReservationActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }
}