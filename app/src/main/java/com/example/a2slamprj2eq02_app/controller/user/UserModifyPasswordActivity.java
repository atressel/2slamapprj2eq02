package com.example.a2slamprj2eq02_app.controller.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.api.API;
import com.example.a2slamprj2eq02_app.controller.LoginActivity;
import com.example.a2slamprj2eq02_app.controller.SettingsActivity;
import com.example.a2slamprj2eq02_app.model.Utilisateur;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserModifyPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_modify_password);

        Button btnModify = findViewById(R.id.btnModify);
        Button btnCancel = findViewById(R.id.btnCancel);

        Utilisateur utilisateur = (Utilisateur) getIntent().getSerializableExtra("user");

        EditText txtLastPassword = findViewById(R.id.editTextLastPassword);
        EditText txtNewPassword = findViewById(R.id.editTextNewPassword);

        btnCancel.setOnClickListener(v -> {
            Intent intent = new Intent(UserModifyPasswordActivity.this, SettingsActivity.class);
            intent.putExtra("user", utilisateur);
            startActivity(intent);
        });

        btnModify.setOnClickListener(v -> {
            String lastPassword = txtLastPassword.getText().toString();
            String newPassword = txtNewPassword.getText().toString();
            if (lastPassword.equals(newPassword)) {
                Toast.makeText(UserModifyPasswordActivity.this, "Veuillez saisir un nouveau mot de passe", Toast.LENGTH_SHORT).show();
            } else if (lastPassword.length() > 1 && newPassword.length() < 1) {
                Toast.makeText(UserModifyPasswordActivity.this, "Veuillez saisir un nouveau mot de passe", Toast.LENGTH_SHORT).show();
            } else if (lastPassword.length() < 1 && newPassword.length() > 1) {
                Toast.makeText(UserModifyPasswordActivity.this, "Veuillez saisir votre ancien mot de passe", Toast.LENGTH_SHORT).show();
            } else if (lastPassword.length() < 1 && newPassword.length() < 1) {
                Toast.makeText(UserModifyPasswordActivity.this, "Veuillez saisir votre ancien et votre nouveau mot de passe", Toast.LENGTH_SHORT).show();
            } else {
                String url = API.buildUrl(API.URL_USERS_MODIFY_PASSWD, "idU=" + utilisateur.getIdU() + "&oldPassword=" + lastPassword + "&newPassword=" + newPassword);
                System.out.println(url);
                Call<JsonObject> callGetResto = API.getJsonData(url);
                callGetResto.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.isSuccessful()) {
                            JsonObject jsonObject = response.body();
                            if (jsonObject.get("success").getAsBoolean()) {
                                Toast.makeText(UserModifyPasswordActivity.this, "Modification de mot de passe effectuée", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(UserModifyPasswordActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                Toast.makeText(UserModifyPasswordActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Toast.makeText(UserModifyPasswordActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}