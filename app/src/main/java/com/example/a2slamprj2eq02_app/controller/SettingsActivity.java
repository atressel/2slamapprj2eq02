package com.example.a2slamprj2eq02_app.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.api.API;
import com.example.a2slamprj2eq02_app.controller.user.UserModifyInfoActivity;
import com.example.a2slamprj2eq02_app.controller.user.UserModifyPasswordActivity;
import com.example.a2slamprj2eq02_app.model.Utilisateur;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatActivity {

    private Utilisateur utilisateur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initializeViews();
    }

    private void initializeViews() {
        ImageView logout = findViewById(R.id.imageLogout);

        TextView txtNom = findViewById(R.id.textViewNom);
        TextView txtPrenom = findViewById(R.id.textViewPrenom);
        TextView txtMail = findViewById(R.id.textViewEmail);
        TextView txtTel = findViewById(R.id.textViewTelephone);
        utilisateur = (Utilisateur) getIntent().getSerializableExtra("user");

        Button btnModifyUser = findViewById(R.id.btnModify);
        Button btnModifyPassword = findViewById(R.id.btnModifyPassword);
        Button btnDeleteUser = findViewById(R.id.btnDeleteAccount);

        txtNom.setText(utilisateur.getNomU());
        txtPrenom.setText(utilisateur.getPrenomU());
        txtMail.setText(utilisateur.getMailU());
        txtTel.setText(utilisateur.getTelU());

        logout.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                android.content.Intent intent = new android.content.Intent(SettingsActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnModifyUser.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                android.content.Intent intent = new android.content.Intent(SettingsActivity.this, UserModifyInfoActivity.class);
                intent.putExtra("user", utilisateur);
                startActivity(intent);
            }
        });

        btnModifyPassword.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                android.content.Intent intent = new android.content.Intent(SettingsActivity.this, UserModifyPasswordActivity.class);
                intent.putExtra("user", utilisateur);
                startActivity(intent);
            }
        });

        btnDeleteUser.setOnClickListener(v -> confirmAndDeleteUser());
    }

    private void confirmAndDeleteUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setTitle("Confirmation");
        builder.setMessage("Voulez-vous vraiment supprimer votre compte ?");
        builder.setPositiveButton("Oui", (dialog, which) -> deleteUser());
        builder.setNegativeButton("Non", (dialog, which) -> dialog.cancel());

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void deleteUser() {
        String url = API.buildUrl(API.URL_USERS_DELETE, "idU=" + utilisateur.getIdU());
        Call<JsonObject> callGetResto = API.getJsonData(url);
        callGetResto.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject jsonObject = response.body();
                    if (jsonObject.get("success").getAsBoolean()) {
                        Toast.makeText(SettingsActivity.this, "Votre compte a bien été supprimé", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        Toast.makeText(SettingsActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(SettingsActivity.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
            }
        });
    }
}