package com.example.a2slamprj2eq02_app.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.adapter.ArrayAdapterResto;
import com.example.a2slamprj2eq02_app.adapter.DownloadImageTask;
import com.example.a2slamprj2eq02_app.api.API;
import com.example.a2slamprj2eq02_app.api.ApiService;
import com.example.a2slamprj2eq02_app.controller.resa.ReservationActivity;
import com.example.a2slamprj2eq02_app.model.Photo;
import com.example.a2slamprj2eq02_app.model.Restaurant;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.Serializable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsRestoActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnResto;
    private Button btnHome;
    private Button resaResto;

    private TextView nomResto;
    private TextView adresseResto;
    private TextView cpResto;
    private TextView telResto;
    private TextView descResto;
    private TextView txtTC;
    private ImageView imgResto;
    private ImageView imgSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_resto);

        initViews();

        int idResto = getIntent().getIntExtra("idResto", 0);

        String url = API.buildUrl(API.URL_RESTAURANTS_GET, "idR=" + idResto);
        Call<JsonObject> callGetResto = API.getJsonData(url);
        callGetResto.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject jsonObject = response.body();
                    boolean success = jsonObject.get("success").getAsBoolean();
                    if (success) {
                        JsonObject resto = jsonObject.getAsJsonObject("resto");

                        String nom = resto.get("nomR").getAsString();
                        String numAddr = resto.get("numAdrR").getAsString();
                        String voieAddrR = resto.get("voieAdrR").getAsString();
                        String cpR = resto.get("cpR").getAsString();
                        String villeR = resto.get("villeR").getAsString();
                        String telR = resto.get("telR").getAsString();
                        String descR = resto.get("descR").getAsString();

                        JsonArray photos = resto.getAsJsonArray("photos");
                        JsonObject photo = photos.get(0).getAsJsonObject();
                        String urlPhoto = photo.get("cheminP").getAsString();

                        JsonArray TypeCuisines = resto.getAsJsonArray("typeCuisines");
                        StringBuilder textTC = new StringBuilder();
                        for (int i = 0; i < TypeCuisines.size(); i++) {
                            JsonObject jsonResto = TypeCuisines.get(i).getAsJsonObject();
                            textTC.append(jsonResto.get("libelleTC").getAsString()).append(" | ");
                        }

                        new DownloadImageTask(imgResto).execute("https://yaguxxxx.fr/PROJETS/resto_gr2/photos/" + urlPhoto);

                        nomResto.setText(nom);
                        adresseResto.setText(numAddr + " " + voieAddrR);
                        cpResto.setText(cpR + " " + villeR);
                        telResto.setText(telR);
                        descResto.setText(descR);
                        txtTC.setText(textTC);
                    } else {
                        Toast.makeText(DetailsRestoActivity.this, "Erreur de connexion", Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(DetailsRestoActivity.this, "Erreur de connexion", Toast.LENGTH_SHORT).show();
            }
        });
        btnResto.setOnClickListener(this);
        btnHome.setOnClickListener(this);
        resaResto.setOnClickListener(this);
        imgSettings.setOnClickListener(this);
    }
    private void initViews() {
        btnResto = findViewById(R.id.detail_btn_restaurants);
        btnHome = findViewById(R.id.detail_btn_home);
        resaResto = findViewById(R.id.btnResa);

        nomResto = findViewById(R.id.detail_name_resto);
        adresseResto = findViewById(R.id.detail_text_addr);
        cpResto = findViewById(R.id.detail_text_cp_ville);
        telResto = findViewById(R.id.detail_text_tel);
        descResto = findViewById(R.id.detail_text_description);
        txtTC = findViewById(R.id.detail_text_TC);

        imgResto = findViewById(R.id.detail_img_resto);
        imgSettings = findViewById(R.id.detail_img_settings);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.detail_btn_restaurants:
                Intent intentResto = new Intent(this, RestaurantsActivity.class);
                intentResto.putExtra("user", getIntent().getSerializableExtra("user"));
                startActivity(intentResto);
                break;
            case R.id.detail_btn_home:
                Intent intentHome = new Intent(this, HomeActivity.class);
                intentHome.putExtra("user", getIntent().getSerializableExtra("user"));
                startActivity(intentHome);
                break;
            case R.id.btnResa:
                Intent intentResa = new Intent(this, ReservationActivity.class);
                intentResa.putExtra("user", getIntent().getSerializableExtra("user"));
                intentResa.putExtra("idResto", getIntent().getIntExtra("idResto", 0));
                startActivity(intentResa);
                break;
            case R.id.detail_img_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                intent.putExtra("user", getIntent().getSerializableExtra("user"));
                startActivity(intent);
                break;
        }
    }
}
