package com.example.a2slamprj2eq02_app.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.api.API;
import com.example.a2slamprj2eq02_app.model.Utilisateur;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etMail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etMail = findViewById(R.id.etEmailRegister);
        etPassword = findViewById(R.id.etPasswordRegister);
        Button btnRegister = findViewById(R.id.btnInscription);

        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnInscription:
                register();
                break;
        }
    }

    private void register() {
        String email = etMail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (email.isEmpty() || password.isEmpty()) {
            etMail.setError("Veuillez remplir tous les champs");
            etPassword.setError("Veuillez remplir tous les champs");
        } else {
            String url = API.buildUrl(API.URL_USERS_REGISTER, "mailU=" + email, "mdpU=" + password);
            System.out.println(url);
            Call<JsonObject> call = API.getJsonData(url);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        JsonObject jsonObject = response.body();
                        boolean success = jsonObject.get("success").getAsBoolean();
                        if (success) {
                            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            // L'inscription a échoué, affichez un message d'erreur
                            Toast.makeText(RegisterActivity.this, "L'inscription a échoué", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    // L'inscription a échoué, affichez un message d'erreur
                    Toast.makeText(RegisterActivity.this, "L'inscription a échoué", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}