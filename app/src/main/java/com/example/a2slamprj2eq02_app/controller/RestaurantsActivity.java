package com.example.a2slamprj2eq02_app.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.adapter.ArrayAdapterResto;
import com.example.a2slamprj2eq02_app.api.API;
import com.example.a2slamprj2eq02_app.model.Photo;
import com.example.a2slamprj2eq02_app.model.Restaurant;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantsActivity extends AppCompatActivity {
    private ListView listView;
    private ImageView imgSettings;
    private Button btnHome;
    private ArrayList<Restaurant> lesResto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        listView = findViewById(R.id.listeResto);
        imgSettings = findViewById(R.id.accueil_img_settings);
        btnHome = findViewById(R.id.btnHome);

        lesResto = new ArrayList<>();
        ArrayAdapterResto adapter = new ArrayAdapterResto(this, lesResto);
        listView.setAdapter(adapter);

        btnHome.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), HomeActivity.class);
            intent.putExtra("user", getIntent().getSerializableExtra("user"));
            startActivity(intent);
        });

        imgSettings.setOnClickListener(v -> {
            Intent intent = new Intent(this, SettingsActivity.class);
            intent.putExtra("user", getIntent().getSerializableExtra("user"));
            startActivity(intent);
        });

        listView.setOnItemClickListener((parent, view, position, id) -> {
            Restaurant resto = lesResto.get(position);
            int idR = resto.getIdR();
            Intent intent = new Intent(RestaurantsActivity.this, DetailsRestoActivity.class);
            intent.putExtra("user", getIntent().getSerializableExtra("user"));
            intent.putExtra("idResto", idR);
            startActivity(intent);
        });

        getRestaurants();
    }

    private void getRestaurants() {
        String url = API.buildUrl(API.URL_RESTAURANTS_GET_ALL);
        Call<JsonObject> call = API.getJsonData(url);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject jsonObject = response.body();
                    boolean success = jsonObject.get("success").getAsBoolean();
                    if (success) {
                        JsonArray jsonRestos = jsonObject.getAsJsonArray("resto");
                        for (JsonElement jsonElement : jsonRestos) {
                            Restaurant resto = new Restaurant(jsonElement.getAsJsonObject());
                            for(JsonElement jsonElement1 : jsonElement.getAsJsonObject().getAsJsonArray("photos")){
                                resto.addPhoto(new Photo(jsonElement1.getAsJsonObject()));
                            }
                            lesResto.add(resto);
                        }
                        ArrayAdapterResto adapter = (ArrayAdapterResto) listView.getAdapter();
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(RestaurantsActivity.this, "Erreur lors de la récupération des restaurants.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RestaurantsActivity.this, "Erreur lors de la récupération des restaurants.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Gérez les erreurs de réseau ici
                Toast.makeText(RestaurantsActivity.this, "Erreur réseau. Veuillez vérifier votre connexion et réessayer.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}