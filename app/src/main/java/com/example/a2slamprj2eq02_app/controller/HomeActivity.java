package com.example.a2slamprj2eq02_app.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.a2slamprj2eq02_app.R;
import com.example.a2slamprj2eq02_app.model.Utilisateur;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnHome, btnResto;
    private ImageView imgSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Utilisateur user = (Utilisateur) getIntent().getSerializableExtra("user");

        imgSettings = findViewById(R.id.accueil_img_settings);
        btnHome = findViewById(R.id.btnHome);
        btnResto = findViewById(R.id.btnRestoHome);

        imgSettings.setOnClickListener(this);
        btnHome.setOnClickListener(this);
        btnResto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Class<?> targetActivity;
        switch (v.getId()) {
            case R.id.btnHome:
                targetActivity = HomeActivity.class;
                break;
            case R.id.btnRestoHome:
                targetActivity = RestaurantsActivity.class;
                break;
            case R.id.accueil_img_settings:
                targetActivity = SettingsActivity.class;
                break;
            default:
                return;
        }
        navigateToActivity(targetActivity);
    }

    private void navigateToActivity(Class<?> targetActivity) {
        Intent intent = new Intent(this, targetActivity);
        intent.putExtra("user", getIntent().getSerializableExtra("user"));
        startActivity(intent);
    }
}