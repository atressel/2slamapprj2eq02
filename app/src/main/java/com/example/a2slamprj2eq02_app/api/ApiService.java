package com.example.a2slamprj2eq02_app.api;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiService {
    @GET
    Call<JsonObject> getJsonData(@Url String url);
}
