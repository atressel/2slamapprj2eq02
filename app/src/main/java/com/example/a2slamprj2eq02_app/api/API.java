package com.example.a2slamprj2eq02_app.api;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {

    private static Retrofit retrofit = null;
    private static ApiService apiService = null;
    public static final String URL = "https://yaguxxxx.fr/PROJETS/resto_gr2/api/";

    public static final String URL_USERS = URL + "Users/";
    public static final String URL_USERS_CONNECT = URL_USERS + "ConnectUser.php";
    public static final String URL_USERS_REGISTER = URL_USERS + "RegisterUser.php";
    public static final String URL_USERS_MODIFY = URL_USERS + "ModifyUser.php";
    public static final String URL_USERS_MODIFY_PASSWD = URL_USERS + "ModifyUserPassword.php";
    public static final String URL_USERS_DELETE = URL_USERS + "DeleteUser.php";

    public static final String URL_RESTAURANTS = URL + "Resto/";
    public static final String URL_RESTAURANTS_GET = URL_RESTAURANTS + "GetOne.php";
    public static final String URL_RESTAURANTS_GET_ALL = URL_RESTAURANTS + "GetAll.php";

    public static final String URL_RESERVATION = URL + "Reservation/";
    public static final String URL_RESERVATION_ADD = URL_RESERVATION + "AddReservation.php";

    public static String buildUrl(String url, String... params) {
        String result = url + "?";
        for (int i = 0; i < params.length; i++) {
            result += params[i];
            if (i < params.length - 1) {
                result += "&";
            }
        }
        return result;
    }

    private static void initRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL) // Base URL, ne pas ajouter le chemin complet ici
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
    }

    private static ApiService getApiService() {
        if (apiService == null) {
            initRetrofit();
            apiService = retrofit.create(ApiService.class);
        }
        return apiService;
    }

    public static Call<JsonObject> getJsonData(String url) {
        ApiService service = getApiService();
        return service.getJsonData(url);
    }
}
