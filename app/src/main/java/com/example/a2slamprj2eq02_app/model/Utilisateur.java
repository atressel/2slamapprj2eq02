package com.example.a2slamprj2eq02_app.model;

import com.google.gson.JsonObject;

import java.io.Serializable;

public class Utilisateur implements Serializable {
    private int idU;
    private String nomU;
    private String prenomU;
    private String mailU;
    private String telU;
    private String mdpU;

    public Utilisateur(int idU, String nomU, String prenomU, String mailU, String telU, String mdpU) {
        this.idU = idU;
        this.nomU = nomU;
        this.prenomU = prenomU;
        this.mailU = mailU;
        this.telU = telU;
        this.mdpU = mdpU;
    }

    public Utilisateur(JsonObject user) {
        this.idU = user.get("idU").isJsonNull() ? 0 : user.get("idU").getAsInt();
        this.nomU = getStringOrDefault(user, "nomU");
        this.prenomU = getStringOrDefault(user, "prenomU");
        this.mailU = getStringOrDefault(user, "mailU");
        this.telU = getStringOrDefault(user, "telU");
        this.mdpU = getStringOrDefault(user, "mdpU");
    }

    private String getStringOrDefault(JsonObject obj, String key) {
        return obj.get(key).isJsonNull() ? "" : obj.get(key).getAsString();
    }

    public int getIdU() {
        return idU;
    }

    public void setIdU(int idU) {
        this.idU = idU;
    }

    public String getNomU() {
        return nomU;
    }

    public void setNomU(String nomU) {
        this.nomU = nomU;
    }

    public String getPrenomU() {
        return prenomU;
    }

    public void setPrenomU(String prenomU) {
        this.prenomU = prenomU;
    }

    public String getMailU() {
        return mailU;
    }

    public void setMailU(String mailU) {
        this.mailU = mailU;
    }

    public String getTelU() {
        return telU;
    }

    public void setTelU(String telU) {
        this.telU = telU;
    }

    public String getMdpU() {
        return mdpU;
    }

    public void setMdpU(String mdpU) {
        this.mdpU = mdpU;
    }
}
