package com.example.a2slamprj2eq02_app.model;

import com.google.gson.JsonObject;

import java.util.ArrayList;

public class Restaurant {
    private int idR;
    private String nomR;
    private String numAdr;
    private String voiAdr;
    private String cpR;
    private String villeR;
    private String latitudeDegR;
    private String longitudeDegR;
    private String descR;
    private String telR;

    private ArrayList<Photo> lesPhotos;
    private ArrayList<TypeCuisine> lesTypesCuisine;

    public Restaurant(int idR, String nomR, String numAdr, String voiAdr, String cpR, String villeR, String latitudeDegR, String longitudeDegR, String descR, String telR) {
        this.idR = idR;
        this.nomR = nomR;
        this.numAdr = numAdr;
        this.voiAdr = voiAdr;
        this.cpR = cpR;
        this.villeR = villeR;
        this.latitudeDegR = latitudeDegR;
        this.longitudeDegR = longitudeDegR;
        this.descR = descR;
        this.telR = telR;
        this.lesPhotos = new ArrayList<Photo>();
        this.lesTypesCuisine = new ArrayList<TypeCuisine>();
    }

    public Restaurant(JsonObject restaurant) {
        this.idR = restaurant.get("idR").getAsInt();
        this.nomR = getStringOrDefault(restaurant, "nomR");
        this.numAdr = getStringOrDefault(restaurant, "numAdrR");
        this.voiAdr = getStringOrDefault(restaurant, "voieAdrR");
        this.cpR = getStringOrDefault(restaurant, "cpR");
        this.villeR = getStringOrDefault(restaurant, "villeR");
        this.latitudeDegR = getStringOrDefault(restaurant, "latitudeDegR");
        this.longitudeDegR = getStringOrDefault(restaurant, "longitudeDegR");
        this.descR = getStringOrDefault(restaurant, "descR");
        this.telR = getStringOrDefault(restaurant, "telR");
        this.lesPhotos = new ArrayList<Photo>();
        this.lesTypesCuisine = new ArrayList<TypeCuisine>();
    }

    private String getStringOrDefault(JsonObject obj, String key) {
        return obj.get(key).isJsonNull() ? "" : obj.get(key).getAsString();
    }

    public int getIdR() {
        return idR;
    }

    public void setIdR(int idR) {
        this.idR = idR;
    }

    public String getNomR() {
        return nomR;
    }

    public void setNomR(String nomR) {
        this.nomR = nomR;
    }

    public String getNumAdr() {
        return numAdr;
    }

    public void setNumAdr(String numAdr) {
        this.numAdr = numAdr;
    }

    public String getVoiAdr() {
        return voiAdr;
    }

    public void setVoiAdr(String voiAdr) {
        this.voiAdr = voiAdr;
    }

    public String getCpR() {
        return cpR;
    }

    public void setCpR(String cpR) {
        this.cpR = cpR;
    }

    public String getVilleR() {
        return villeR;
    }

    public void setVilleR(String villeR) {
        this.villeR = villeR;
    }

    public String getLatitudeDegR() {
        return latitudeDegR;
    }

    public void setLatitudeDegR(String latitudeDegR) {
        this.latitudeDegR = latitudeDegR;
    }

    public String getLongitudeDegR() {
        return longitudeDegR;
    }

    public void setLongitudeDegR(String longitudeDegR) {
        this.longitudeDegR = longitudeDegR;
    }

    public String getDescR() {
        return descR;
    }

    public void setDescR(String descR) {
        this.descR = descR;
    }

    public String getTelR() {
        return telR;
    }

    public void setTelR(String telR) {
        this.telR = telR;
    }

    public ArrayList<Photo> getLesPhotos() {
        return lesPhotos;
    }

    public void setLesPhotos(ArrayList<Photo> lesPhotos) {
        this.lesPhotos = lesPhotos;
    }

    public ArrayList<TypeCuisine> getLesTypesCuisine() {
        return lesTypesCuisine;
    }

    public void setLesTypesCuisine(ArrayList<TypeCuisine> lesTypesCuisine) {
        this.lesTypesCuisine = lesTypesCuisine;
    }

    public void addPhoto(Photo photo) {
        this.lesPhotos.add(photo);
    }
}
