package com.example.a2slamprj2eq02_app.model;

public class TypeCuisine {
    private int idTC;
    private String libelleTC;

    public TypeCuisine(int idTC, String libelleTC) {
        this.idTC = idTC;
        this.libelleTC = libelleTC;
    }

    public int getIdTC() {
        return idTC;
    }

    public void setIdTC(int idTC) {
        this.idTC = idTC;
    }

    public String getLibelleTC() {
        return libelleTC;
    }

    public void setLibelleTC(String libelleTC) {
        this.libelleTC = libelleTC;
    }
}
