package com.example.a2slamprj2eq02_app.model;

import com.google.gson.JsonObject;

public class Photo {
    private int idP;
    private String cheminP;

    public Photo(int idP, String cheminP) {
        this.idP = idP;
        this.cheminP = cheminP;
    }

    public Photo(JsonObject asJsonObject) {
        this.idP = asJsonObject.get("idP").getAsInt();
        this.cheminP = asJsonObject.get("cheminP").getAsString();
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getCheminP() {
        return cheminP;
    }

    public void setCheminP(String cheminP) {
        this.cheminP = cheminP;
    }
}
